# coding=utf-8
# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

yourpath = '/Users/janthip/PycharmProjects/GenerateFile/venv'
newpath = '/Users/janthip/PycharmProjects/GenerateFile/venv/NewFiles'

import os
import requests
import json
import shutil

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print("Hi, {0}".format(name))  # Press ⌘F8 to toggle the breakpoint.

    response = requests.get("https://medhr.medicine.psu.ac.th/app-api/v2/?/apis/cardnew/request_view/request_types_id/1/status_id/0/?limit=9999")

    # jprint(response.json())

    # Directory
    directory = "NewFiles"

    # Parent Directory path
    parent_dir = "/Users/janthip/PycharmProjects/GenerateFile/venv/"

    # Path
    path = os.path.join(parent_dir, directory)

    # Create the directory
    # 'GeeksForGeeks' in
    # '/home / User / Documents'
    # os.mkdir(path)
    # print("Directory '% s' created" % directory)

    for root, dirs, files in os.walk(yourpath, topdown=False):

        for filename in files:

            if filename.find('docker') >= 0:
                # I use absolute path, case you want to move several dirs.
                old_name = os.path.join(os.path.abspath(root), filename)

                # Separate base from extension
                base, extension = os.path.splitext(filename)

                # Initial new name
                new_name = os.path.join(newpath, filename)

                # If folder basedir/base does not exist... You don't want to create it?
                if os.path.exists(os.path.join(newpath, base)):
                    print os.path.join(newpath, base), "not found"
                    continue  # Next filename
                elif not os.path.exists(new_name):  # folder exists, file does not
                    shutil.copy(old_name, new_name)
                else:  # folder exists, file exists as well
                    print  base + ' is already'
                    break
                    # ii = 1
                    # while True:
                    #     new_name = os.path.join(newpath, base + "_" + str(ii) + extension)
                    #     if not os.path.exists(new_name):
                    #         shutil.copy(old_name, new_name)
                    #         print "Copied", old_name, "as", new_name
                    #         break
                    #     ii += 1


        # for name in files:
        #
        #     if name.find('docker') >= 0:
        #         filename, file_extension = os.path.splitext(os.path.join(root, name))
        #         print filename
        #         print(file_extension)
        #         os.rename(os.path.join(root, name), root + '/' + 'docker' + file_extension)

        # for name in files:
        #     print(os.path.join(root, name))

        # for name in dirs:
        #     print(os.path.join(root, name))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
